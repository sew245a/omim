package com.mapswithme.maps;

import android.content.ContentResolver;
import android.content.Context;
import android.net.Uri;
import android.os.ParcelFileDescriptor;
import android.provider.DocumentsContract;
import android.util.Log;

import com.github.axet.androidlibrary.app.Storage;

import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.channels.FileChannel;
import java.util.ArrayList;

public class SAF {
    public static final String TAG = SAF.class.getSimpleName();

    public static SAF SAF = new SAF();

    Context context;
    ArrayList<Storage.Node> nn;

    public static void init(Context context) {
        SAF.context = context;
    }

    public static Uri parse(String uri) {
        Uri u;
        if (uri.startsWith(ContentResolver.SCHEME_CONTENT)) {
            u = Uri.parse(uri);
            if (!DocumentsContract.isDocumentUri(SAF.context, u)) { // tree uri?
                Uri u1 = Storage.getDocumentTreeUri(u);
                if (!u.equals(u1)) { // broken uri. tree uri + last path segment
                    String last = u.getLastPathSegment();
                    u = Storage.getDocumentChild(SAF.context, u1, last);
                }
            }
        } else if (uri.startsWith(ContentResolver.SCHEME_FILE)) {
            u = Uri.parse(uri);
        } else {
            u = Uri.parse(ContentResolver.SCHEME_FILE + Storage.CSS + uri);
        }
        return u;
    }

    synchronized public static String FilesName(int i) {
        return SAF.nn.get(i).name;
    }

    synchronized public static int FilesCount(String dir) {
        Log.d(TAG, "FilesCount " + dir);
        try {
            Uri u = parse(dir);
            SAF.nn = Storage.list(SAF.context, u);
            return SAF.nn.size();
        } catch (RuntimeException e) {
            Log.w(TAG, e);
            return 0;
        }
    }

    synchronized public static int FilesType(String path) {
        Log.d(TAG, "FilesType " + path);
        return 0;
    }

    synchronized public static long FilesSize(String path) {
        Log.d(TAG, "FilesSize " + path);
        return Storage.getLength(SAF.context, parse(path));
    }

    synchronized public static int FilesWrite(String path, long off, byte[] buf, int pos, int size) {
        Log.d(TAG, "FilesWrite " + path + " " + off + " " + size);
        if (size == 0)
            return 0;
        Uri u = parse(path);
        String s = u.getScheme();
        if (s.equals(ContentResolver.SCHEME_CONTENT)) {
            ParcelFileDescriptor fd = null;
            FileOutputStream fos = null;
            FileChannel c = null;
            try {
                if (!Storage.isDocumentExists(SAF.context, u)) {
                    String name = Storage.getDocumentName(SAF.context, u);
                    Uri p = Storage.getDocumentParent(SAF.context, u);
                    u = Storage.createDocumentFile(SAF.context, p, name);
                }
                ContentResolver resolver = SAF.context.getContentResolver();
                fd = resolver.openFileDescriptor(u, "rw");
                fos = new FileOutputStream(fd.getFileDescriptor());
                c = fos.getChannel();
                c.position(off);
                ByteBuffer bb = ByteBuffer.wrap(buf, pos, size);
                return c.write(bb);
            } catch (IOException e) {
                throw new RuntimeException(e);
            } finally {
                IOUtils.closeQuietly(c, fos, fd);
            }
        } else {
            try {
                RandomAccessFile r = new RandomAccessFile(Storage.getFile(u), "rw");
                r.seek(off);
                r.write(buf, pos, size);
                r.close();
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
        return size;
    }

    synchronized public static int FilesRead(String path, long off, byte[] buf, int pos, int size) {
        Log.d(TAG, "FilesRead " + path + " " + off + " " + size);
        if (size == 0)
            return 0;
        Uri u = parse(path);
        String s = u.getScheme();
        if (s.equals(ContentResolver.SCHEME_CONTENT)) {
            ParcelFileDescriptor fd = null;
            FileInputStream fos = null;
            FileChannel c = null;
            try {
                ContentResolver resolver = SAF.context.getContentResolver();
                fd = resolver.openFileDescriptor(u, "rw");
                fos = new FileInputStream(fd.getFileDescriptor());
                c = fos.getChannel();
                c.position(off);
                ByteBuffer bb = ByteBuffer.wrap(buf, pos, size);
                return c.read(bb);
            } catch (IOException e) {
                throw new RuntimeException(e);
            } finally {
                IOUtils.closeQuietly(c, fos, fd);
            }
        } else {
            try {
                RandomAccessFile r = new RandomAccessFile(Storage.getFile(u), "rw");
                r.seek(off);
                int d = r.read(buf, pos, size);
                r.close();
                return d;
            } catch (IOException e) {
                throw new RuntimeException(e);
            }
        }
    }

    synchronized public static boolean FilesDelete(String path) {
        Log.d(TAG, "FilesDelete " + path);
        Uri uri = parse(path);
        String s = uri.getScheme();
        if (s.equals(ContentResolver.SCHEME_FILE)) {
            File f = Storage.getFile(uri);
            if (f.isDirectory()) {
                try {
                    FileUtils.cleanDirectory(f);
                    return true;
                } catch (IOException ignore) {
                    return false;
                }
            } else
                return FileUtils.deleteQuietly(f);
        } else {
            try {
                return Storage.delete(SAF.context, uri); // single file, recursion not supported
            } catch (Throwable e) {
                Log.w(TAG, e);
                return false;
            }
        }
    }

    synchronized public static boolean FilesMove(String from, String to) { // single file/folder rename, full featured move (folders, different saf sources) not supported
        Log.d(TAG, "FilesMove " + from + " " + to);
        Uri f = parse(from);
        Uri t = parse(to);
        String name = Storage.getName(SAF.context, t);
        String s = f.getScheme();
        String s2 = t.getScheme();
        Uri r;
        if (s.equals(ContentResolver.SCHEME_FILE) && s2.equals(ContentResolver.SCHEME_FILE)) // file -> file
            r = Uri.fromFile(Storage.move(Storage.getFile(f), Storage.getFile(t)));
        else if (s.equals(ContentResolver.SCHEME_FILE)) { // file -> saf
            Uri p = Storage.getParent(SAF.context, t);
            r = Storage.move(SAF.context, Storage.getFile(f), p, name);
        } else {
            Uri t1 = Storage.getDocumentTreeUri(f);
            Uri t2 = Storage.getDocumentTreeUri(t);
            if (t1.equals(t2)) { // only supporting move/rename from same SAF locations
                try {
                    Storage.delete(SAF.context, t); // delete target file first
                } catch (RuntimeException ignore) {
                }
                r = Storage.rename(SAF.context, f, name); // rename file
            } else {
                return false;
            }
        }
        return r != null;
    }

    synchronized public static boolean FilesMkdir(String path) {
        Log.d(TAG, "FilesMkdir " + path);
        return false;
    }
}


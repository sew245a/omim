package com.mapswithme.maps.downloader;

import android.os.AsyncTask;
import android.util.Base64;
import android.net.Uri;

import com.mapswithme.util.Constants;
import com.mapswithme.util.HttpClient;
import com.mapswithme.util.StringUtils;
import com.mapswithme.util.Utils;
import com.mapswithme.util.log.Logger;
import com.mapswithme.util.log.LoggerFactory;

import java.io.BufferedInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.util.Map;
import java.util.concurrent.Executor;
import java.util.concurrent.Executors;

@SuppressWarnings("unused")
class ChunkTask extends AsyncTask<Void, byte[], Integer>
{
  private static final Logger LOGGER = LoggerFactory.INSTANCE.getLogger(LoggerFactory.Type.DOWNLOADER);
  private static final String TAG = "ChunkTask";

  private static final int TIMEOUT_IN_SECONDS = 60;

  final long mHttpCallbackID;
  final String mUrl;
  final long mBeg;
  final long mEnd;
  final long mExpectedFileSize;
  byte[] mPostBody;
  final String mUserAgent;

  public static final int IO_EXCEPTION = -1;
  public static final int WRITE_EXCEPTION = -2;
  public static final int INCONSISTENT_FILE_SIZE = -3;
  public static final int NON_HTTP_RESPONSE = -4;
  public static final int INVALID_URL = -5;
  public static final int CANCELLED = -6;

  long mDownloadedBytes;

  private static final Executor sExecutors = Executors.newFixedThreadPool(4);

  public ChunkTask(long httpCallbackID, String url, long beg, long end,
                   long expectedFileSize, byte[] postBody, String userAgent)
  {
    mHttpCallbackID = httpCallbackID;
    mUrl = url;
    mBeg = beg;
    mEnd = end;
    mExpectedFileSize = expectedFileSize;
    mPostBody = postBody;
    mUserAgent = userAgent;
  }

  @Override
  protected void onPreExecute() {}

  private long getChunkID()
  {
    return mBeg;
  }

  @Override
  protected void onPostExecute(Integer httpOrErrorCode)
  {
    //Log.i(TAG, "Writing chunk " + getChunkID());

    // It seems like onPostExecute can be called (from GUI thread queue)
    // after the task was cancelled in destructor of HttpThread.
    // Reproduced by Samsung testers: touch Try Again for many times from
    // start activity when no connection is present.

    if (!isCancelled())
      onFinish(mHttpCallbackID, httpOrErrorCode, mBeg, mEnd);
  }

  @Override
  protected void onProgressUpdate(byte[]... data)
  {
    if (!isCancelled())
    {
      // Use progress event to save downloaded bytes.
      if (nativeOnWrite(mHttpCallbackID, mBeg + mDownloadedBytes, data[0], data[0].length))
        mDownloadedBytes += data[0].length;
      else
      {
        // Cancel downloading and notify about error.
        cancel(false);
        onFinish(mHttpCallbackID, WRITE_EXCEPTION, mBeg, mEnd);
      }
    }
  }

  public void onFinish(long id, int err, long beg, long end) {
    nativeOnFinish(id, err, beg, end);
  }

  void start()
  {
    if (mPostBody != null || mUrl.isEmpty() || mUrl.startsWith("http") || !DownloadManager.DM.start(this))
      executeOnExecutor(sExecutors, (Void[]) null);
  }

  @Override
  protected void onCancelled(Integer integer) {
    DownloadManager.DM.cancel(this);
  }

  public static long parseContentRange(String contentRangeValue)
  {
    if (contentRangeValue != null)
    {
      final int slashIndex = contentRangeValue.lastIndexOf('/');
      if (slashIndex >= 0)
      {
        try
        {
          return Long.parseLong(contentRangeValue.substring(slashIndex + 1));
        } catch (final NumberFormatException ex)
        {
          // Return -1 at the end of function
        }
      }
    }
    return -1;
  }

  @Override
  protected Integer doInBackground(Void... p)
  {
    return DownloadManager.DM.doInBackground(this);
  }

  protected Integer downloadFromStream(InputStream stream)
  {
    // Because of timeouts in InputStream.read (for bad connection),
    // try to introduce dynamic buffer size to read in one query.
    final int arrSize[] = {64, 32, 1};
    int ret = IO_EXCEPTION;

    for (int size : arrSize)
    {
      try
      {
        ret = downloadFromStreamImpl(stream, size * Constants.KB);
        break;
      } catch (final IOException ex)
      {
        LOGGER.e(TAG, "IOException in downloadFromStream for chunk size: " + size, ex);
      }
    }

    Utils.closeSafely(stream);
    return ret;
  }

  /**
   * @throws IOException
   */
  private int downloadFromStreamImpl(InputStream stream, int bufferSize) throws IOException
  {
    final byte[] tempBuf = new byte[bufferSize];

    int readBytes;
    while ((readBytes = stream.read(tempBuf)) > 0)
    {
      if (isCancelled())
        return CANCELLED;

      final byte[] chunk = new byte[readBytes];
      System.arraycopy(tempBuf, 0, chunk, 0, readBytes);

      publishProgress(chunk);
    }

    // -1 - means the end of the stream (success), else - some error occurred
    return (readBytes == -1 ? HttpURLConnection.HTTP_OK : IO_EXCEPTION);
  }

  static native boolean nativeOnWrite(long httpCallbackID, long beg, byte[] data, long size);
  static native void nativeOnFinish(long httpCallbackID, long httpCode, long beg, long end);
}

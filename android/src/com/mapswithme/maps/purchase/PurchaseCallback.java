package com.mapswithme.maps.purchase;

import androidx.annotation.NonNull;

import java.util.List;

public interface PurchaseCallback
{
  void onValidationFinish(boolean success);
}

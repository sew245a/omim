/*******************************************************************************
 * The MIT License (MIT)
 * <p/>
 * Copyright (c) 2014 Alexander Zolotarev <me@alex.bio> from Minsk, Belarus
 * <p/>
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 * <p/>
 * The above copyright notice and this permission notice shall be included in all
 * copies or substantial portions of the Software.
 * <p/>
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN THE
 * SOFTWARE.
 *******************************************************************************/

package com.mapswithme.util;

import android.os.Build;
import android.text.TextUtils;

import androidx.annotation.NonNull;

import com.mapswithme.maps.downloader.DownloadManager;
import com.mapswithme.util.log.Logger;
import com.mapswithme.util.log.LoggerFactory;

import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLSocketFactory;
import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.HttpURLConnection;
import java.net.URL;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.zip.GZIPInputStream;
import java.util.zip.InflaterInputStream;

public final class HttpClient
{
  public static final String HEADER_USER_AGENT = "User-Agent";
  public static final String HEADER_AUTHORIZATION = "Authorization";
  public static final String HEADER_BEARER_PREFFIX = "Bearer ";
  public static final String HEADER_BUNDLE_TIERS = "X-Mapsme-Bundle-Tiers";
  public static final String HEADER_THEME_KEY = "x-mapsme-theme";
  public static final String HEADER_THEME_DARK = "dark";
  private final static String TAG = HttpClient.class.getSimpleName();
  // TODO(AlexZ): tune for larger files
  private final static int STREAM_BUFFER_SIZE = 1024 * 64;
  private static final Logger LOGGER = LoggerFactory.INSTANCE.getLogger(LoggerFactory.Type.NETWORK);

  public static Params run(@NonNull final Params p) throws IOException, NullPointerException
  {
    return DownloadManager.DM.download(p);
  }

  private static void setupTLSForPreLollipop(@NonNull HttpURLConnection connection)
  {
    // On PreLollipop devices we use the custom ssl factory which enables TLSv1.2 forcibly, because
    // TLS of the mentioned version is not enabled by default on PreLollipop devices, but some of
    // used by us APIs (as Viator) requires TLSv1.2. For more info see
    // https://developer.android.com/reference/javax/net/ssl/SSLEngine.html.
    if ((connection instanceof HttpsURLConnection)
        && Build.VERSION.SDK_INT < Build.VERSION_CODES.LOLLIPOP)
    {
      HttpsURLConnection sslConnection = (HttpsURLConnection) connection;
      SSLSocketFactory factory = sslConnection.getSSLSocketFactory();
      sslConnection.setSSLSocketFactory(new PreLollipopSSLSocketFactory(factory));
    }
  }

  @NonNull
  private static InputStream getInputStream(@NonNull HttpURLConnection connection) throws IOException
  {
    InputStream in;
    try
    {
      if ("gzip".equals(connection.getContentEncoding()))
        in = new GZIPInputStream(connection.getInputStream());
      else if ("deflate".equals(connection.getContentEncoding()))
        in = new InflaterInputStream(connection.getInputStream());
      else
        in = connection.getInputStream();
    }
    catch (IOException e)
    {
      in = connection.getErrorStream();
      if (in == null)
        throw e;
    }
    return in;
  }

  public static class Params
  {
    public void setHeaders(@NonNull KeyValue[] array)
    {
      headers = new ArrayList<>(Arrays.asList(array));
    }

    public Object[] getHeaders()
    {
      return headers.toArray();
    }

    public String url;
    // Can be different from url in case of redirects.
    public String receivedUrl;
    public String httpMethod;
    // Should be specified for any request whose method allows non-empty body.
    // On return, contains received Content-Type or null.
    // Can be specified for any request whose method allows non-empty body.
    // On return, contains received Content-Encoding or null.
    public byte[] data;
    // Send from input file if specified instead of data.
    public String inputFilePath;
    // Received data is stored here if not null or in data otherwise.
    public String outputFilePath;
    public String cookies;
    public ArrayList<KeyValue> headers = new ArrayList<>();
    public int httpResponseCode = -1;
    public boolean followRedirects = true;
    public boolean loadHeaders;
    public int timeoutMillisec = Constants.READ_TIMEOUT_MS;

    // Simple GET request constructor.
    public Params(String url)
    {
      this.url = url;
      httpMethod = "GET";
    }
  }
}

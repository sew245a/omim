#!/usr/bin/env python

import sys
import json
import urllib

dic={}

apache=json.load(sys.stdin)
for k in apache:
    v = apache[k]
    id=urllib.unquote(k)
    dic[id]=v

def printg(data):
    global dic
    for g in data['g']:
        id=g['id']+".mwm"
        if 'g' in g:
            printg(g)
        else:
            g['s']=dic[id]['size']

data = None

with open('../data/countries.txt') as json_file:  
    data = json.load(json_file)
    printg(data)

with open('../data/countries.txt', 'w') as outfile:  
    json.dump(data, outfile, indent=2)

external={}

with open('../data/external_resources.txt') as f:
    external = f.readlines()

with open('../data/external_resources.txt', 'w') as f:  
    for e in external:
        xx=e.split()
        n=xx[0]
        s=xx[1]
        if n in dic:
            s=dic[n]['size']
        f.write("%s %s\n" % (n, s))
